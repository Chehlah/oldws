#!/usr/bin/env python

import rospy
import sys
import select, termios, tty
import geometry_msgs.msg

# input value to each paramiter
def move(speed,angle):
    cmd = geometry_msgs.msg.Twist() 
    cmd.linear.x = speed
    cmd.linear.y = 0.0
    cmd.linear.z = 0.0
    cmd.angular.x = 0.0
    cmd.angular.y = 0.0
    cmd.angular.z = angle
    return cmd

# main 
def arrow():
    count = 0
    while not rospy.is_shutdown():
        # loop for send data 
        if count < 4 : 
            #publish topic
            pub.publish(move(2,-1.8))
            pub1.publish(move(2,1.8))
            count = count + 1
        else:
            count = 0
        rate.sleep()

if __name__ == "__main__":
    try:
        #set node name
        rospy.init_node('mirror_bot')
        #set publish topic name
        pub = rospy.Publisher('/turtlesim1/turtle2/cmd_vel', geometry_msgs.msg.Twist, queue_size=5)
        pub1 = rospy.Publisher('/turtlesim2/turtle1/cmd_vel', geometry_msgs.msg.Twist, queue_size=5)
        rate = rospy.Rate(10)
        #while main loop 
        while(1):
            arrow()
    except rospy.ROSInterruptException:
        pass