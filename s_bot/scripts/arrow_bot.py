#!/usr/bin/env python

import rospy
import sys
import select, termios, tty
import geometry_msgs.msg

msg = """
Control Your Turtlebot!
---------------------------
Moving around:
   7    8    9
   4    5    6
   1    2    3

force stop: 5

CTRL-C to quit
"""
# dictionary for eche number
# keySet = {
#         '8':(1,0),
#         '9':(1,-1),
#         '4':(0,1),
#         '6':(0,-1),
#         '7':(1,1),
#         '2':(-1,0),
#         '3':(-1,1),
#         '1':(-1,-1),
#            }
keySet = {
        '\x1b[A':(1,0),
        '9':(1,-1),
        '\x1b[D':(0,1),
        '\x1b[C':(0,-1),
        '7':(1,1),
        '\x1b[B':(-1,0),
        '3':(-1,1),
        '1':(-1,-1),
           }

# class _Getch:
#     def __call__(self):
#             fd = sys.stdin.fileno()
#             old_settings = termios.tcgetattr(fd)
#             try:
#                 tty.setraw(sys.stdin.fileno())
#                 ch = sys.stdin.read(3)
#             finally:
#                 termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
#             return ch
#set paramiter 
def move(speed,dist):
    cmd = geometry_msgs.msg.Twist() 
    cmd.linear.x = speed
    cmd.linear.y = 0.0
    cmd.linear.z = 0.0
    cmd.angular.x = 0.0
    cmd.angular.y = 0.0
    cmd.angular.z = dist
    return cmd
# input from keyboard just 1 charecter
# def _getch():
#         fd = sys.stdin.fileno()
#         old_settings = termios.tcgetattr(fd)
#         try:
#             tty.setraw(fd)
#             ch = sys.stdin.read(1)
#         finally:
#             termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
#         return ch
def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(3)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

# main work
def arrow():
    count = 0
    input_key = _getch()
    if input_key in keySet:
        speed = keySet[input_key] 
        #find number in dictionary
        
    else:
        speed = 0.0
        dist = 0.0
    #send data to set velue before publish
    pub_move = move(speed[0],speed[1])

    #publish 4 times equal 1 time ()
    while not rospy.is_shutdown():
        if count < 4 :
            pub.publish(pub_move)
            count = count + 1
        else:
            break
        rate.sleep()

if __name__ == "__main__":
    try:
        print msg
        rospy.init_node('arrow_bot')
        pub = rospy.Publisher('/cmd_vel_mux/input/teleop', geometry_msgs.msg.Twist, queue_size=5) #/cmd_vel_mux/input/teleop
        rate = rospy.Rate(10)
        while(1):
            arrow()
    except rospy.ROSInterruptException:
        pass