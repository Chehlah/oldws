#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Joy.h"
#include <sstream>

geometry_msgs::Twist cmd;
ros::Publisher pub,pub1;
ros::Subscriber sub;
void joy_callback(const sensor_msgs::Joy& msg) //call function that recieve value from sensor_msgs
{
    //sensor_msgs for direction of turtlesim 
    //forward
    if(msg.axes[5]==1)				
    {
      cmd.linear.x = 2.0;
    }
    //backward
    else if(msg.axes[5]==-1)
    {
      cmd.linear.x = -2.0;
    }
    //turn right
    else if(msg.axes[4]==1)
    {
      cmd.angular.z = 1.8;
    }
    //turn left
    else if(msg.axes[4]==-1)
    {
      cmd.angular.z = -1.8;
    }
    //default
    else
    {
      cmd.linear.x = 0;
      cmd.angular.z = 0;
    }
}
int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "joy_bot_base"); //define type node
  ros::NodeHandle n;  
  pub = n.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/teleop", 1000); //send publish value to turtlesim 
  pub1 = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);
  sub = n.subscribe("/joy", 1000, joy_callback); //subcribe by joy
    while (ros::ok())
  {
    pub.publish(cmd);
    pub1.publish(cmd);
    ros::spinOnce();
  }
    ros::spin();
  return 0;
}
