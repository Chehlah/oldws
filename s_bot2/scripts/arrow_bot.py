#!/usr/bin/env python

import rospy
import sys
import select, termios, tty
import geometry_msgs.msg

msg = """
Control Your Turtlebot!
---------------------------
Moving around:
   7    ^    9
   <    5    >
   1    v    3

force stop: 5

CTRL-C to quit
"""  
# Dictionary of keyboard          
keySet = {
        '\x1b[A':(1,0),
        '9':(1,-1),
        '\x1b[D':(0,1),
        '\x1b[C':(0,-1),
        '7':(1,1),
        '\x1b[B':(-1,0),
        '3':(-1,1),
        '1':(-1,-1),
           }
# set value for each paramiter 
def move(speed,angle):
    cmd = geometry_msgs.msg.Twist() 
    cmd.linear.x = speed
    cmd.linear.y = 0.0
    cmd.linear.z = 0.0
    cmd.angular.x = 0.0
    cmd.angular.y = 0.0
    cmd.angular.z = angle
    return cmd
# input from keyboard just 1 charecter
def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(3)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
# main work
def arrow():
    count = 0
    # input from keyboard
    input_key = _getch()
    # find number in dictionary of keyboard
    if input_key in keySet:
        speed = keySet[input_key]      
    else:
        speed = [0.0,0.0]
    # send data to set velue before publish
    pub_move = move(speed[0],speed[1])
    # publish 4 times equal 1 time
    while not rospy.is_shutdown():
        if count < 4 :
            pub.publish(pub_move)
            count = count + 1
        else:
            break
        rate.sleep()

if __name__ == "__main__":
    try:
        print msg
        # set node name
        rospy.init_node('arrow_bot')
        # set topic name for publish
        pub = rospy.Publisher('/cmd_vel_mux/input/teleop', geometry_msgs.msg.Twist, queue_size=5)
        rate = rospy.Rate(10)
        while(1):
            arrow()
    except rospy.ROSInterruptException:
        pass