import rospy
import sys

# main work
def de():
    count = 0
    while not rospy.is_shutdown():
        if count < 4 :
            count = count + 1
        else:
            break
        rate.sleep()

if __name__ == "__main__":
    try:
        rospy.init_node('de_time')
        rate = rospy.Rate(100)
        de()
    except rospy.ROSInterruptException:
        pass