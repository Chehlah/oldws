// This code using with RX-64
// control in wheel mode
// last edit: 2016, November 14

#ifdef __linux__
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <conio.h>
#endif

#include <stdlib.h>
#include <stdio.h>

// Uses Dynamixel SDK library
#ifndef DYNAMIXEL_SDK_INCLUDE_DYNAMIXEL_SDK_DYNAMIXELSDK_H_
#define DYNAMIXEL_SDK_INCLUDE_DYNAMIXEL_SDK_DYNAMIXELSDK_H_


#include "dynamixel_sdk/group_bulk_read.h"
#include "dynamixel_sdk/group_bulk_write.h"
#include "dynamixel_sdk/group_sync_read.h"
#include "dynamixel_sdk/group_sync_write.h"
#include "dynamixel_sdk/protocol1_packet_handler.h"
#include "dynamixel_sdk/protocol2_packet_handler.h"

#ifdef __linux__
  #include "dynamixel_sdk/port_handler_linux.h"
#endif

#if defined(_WIN32) || defined(_WIN64)
  #include "dynamixel_sdk/port_handler_windows.h"
#endif


#endif 

#include "ros/ros.h"

// Control table address for RX-64
#define ADDR_RX_TORQUE_ENABLE           24                 
#define ADDR_RX_GOAL_POSITION           30
#define ADDR_RX_PRESENT_LOAD            40
#define ADDR_RX_MOVING_SPEED            32
#define ADDR_RX_CW_LIMIT                6
#define ADDR_RX_CCW_LIMIT               8

// Protocol version
#define PROTOCOL_VERSION                1.0                 // See which protocol version is used in the Dynamixel

// Default setting
#define DXL_ID                          1                   // Dynamixel ID: 1
#define BAUDRATE                        1000000
#define DEVICENAME                      "/dev/ttyUSB0"      // Check which port is being used on your controller
                                                            // ex) Windows: "COM1"   Linux: "/dev/ttyUSB0"

#define TORQUE_ENABLE                   1                   // Value for enabling the torque
#define TORQUE_DISABLE                  0                   // Value for disabling the torque
#define DXL_MINIMUM_POSITION_VALUE      0                   // Dynamixel will rotate between this value
#define DXL_MAXIMUM_POSITION_VALUE      1023                // and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
#define DXL_CW_CCW_LIMIT                0                   // Dynamixel can be applied to move wheels.


#define ESC_ASCII_VALUE                 0x1b

int getch()
{
#ifdef __linux__
  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;
#elif defined(_WIN32) || defined(_WIN64)
  return _getch();
#endif
}

int kbhit(void)
{
#ifdef __linux__
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if (ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
#elif defined(_WIN32) || defined(_WIN64)
  return _kbhit();
#endif
}

int main()
{
  // Initialize PortHandler instance
  // Set the port path
  // Get methods and members of PortHandlerLinux or PortHandlerWindows
  dynamixel::PortHandler *portHandler = dynamixel::PortHandler::getPortHandler(DEVICENAME);

  // Initialize PacketHandler instance
  // Set the protocol version
  // Get methods and members of Protocol1PacketHandler or Protocol2PacketHandler
  dynamixel::PacketHandler *packetHandler = dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION);

  int dxl_comm_result = COMM_TX_FAIL;             // Communication result
  int dxl_turn_direction = 2;                     // Direction control CCW:0 CW:1
  int dxl_speed[3] = {512,1536,0};                  //CCW 50%:512  CW 50%:1536 
  
  uint8_t dxl_error = 0;                          // Dynamixel error
  uint16_t dxl_present_load = 0;                  // Present Load               

  // Open port
  if (portHandler->openPort())
  {
    printf("Succeeded to open the port!\n");
  }
  else
  {
    printf("Failed to open the port!\n");
    printf("Press any key to terminate...\n");
    getch();
    return 0;
  }

  // Set port baudrate
  if (portHandler->setBaudRate(BAUDRATE))
  {
    printf("Succeeded to change the baudrate!\n");
  }
  else
  {
    printf("Failed to change the baudrate!\n");
    printf("Press any key to terminate...\n");
    getch();
    return 0;
  }

  // Enable Dynamixel Torque
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL_ID, ADDR_RX_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->printTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->printRxPacketError(dxl_error);
  }
  else
  {
    printf("Dynamixel has been successfully connected \n");
  }

  //Set to wheel mode
  dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_CW_LIMIT , DXL_CW_CCW_LIMIT, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
  else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    }
  dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_CCW_LIMIT , DXL_CW_CCW_LIMIT, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
  else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    }
  dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_MOVING_SPEED, dxl_speed[2], &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
  else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    }

  while(1)
  {
      //Input direction CCW=0 CW=1 Stop=2
      printf("Input CCW:0 CW:1 Stop:2 (Other value to quit!)\n");
      scanf(" %d",&dxl_turn_direction);
      if(dxl_turn_direction != 0 && dxl_turn_direction != 1 && dxl_turn_direction != 2)
      {
          break;
      }

      //Run a wheel
      dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_MOVING_SPEED, dxl_speed[dxl_turn_direction], &dxl_error);
      if (dxl_comm_result != COMM_SUCCESS)
      {
        packetHandler->printTxRxResult(dxl_comm_result);
      }
      else if (dxl_error != 0)
      {
        packetHandler->printRxPacketError(dxl_error);
      }
  
  }

  // Disable Dynamixel Torque
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL_ID, ADDR_RX_TORQUE_ENABLE, TORQUE_DISABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    packetHandler->printTxRxResult(dxl_comm_result);
  }
  else if (dxl_error != 0)
  {
    packetHandler->printRxPacketError(dxl_error);
  }

  //Set to joint mode
  dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_CW_LIMIT , DXL_MINIMUM_POSITION_VALUE, &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
    else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    }
  dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_CCW_LIMIT , DXL_MAXIMUM_POSITION_VALUE, &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
    else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    }

    //write goal position to middle
    dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_MOVING_SPEED, 50, &dxl_error);
      if (dxl_comm_result != COMM_SUCCESS)
      {
        packetHandler->printTxRxResult(dxl_comm_result);
      }
      else if (dxl_error != 0)
      {
        packetHandler->printRxPacketError(dxl_error);
      }
    dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL_ID, ADDR_RX_GOAL_POSITION , 512, &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS)
    {
      packetHandler->printTxRxResult(dxl_comm_result);
    }
    else if (dxl_error != 0)
    {
      packetHandler->printRxPacketError(dxl_error);
    } 

  // Close port
  portHandler->closePort();

  return 0;
}
