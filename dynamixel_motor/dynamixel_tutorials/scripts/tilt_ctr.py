#!/usr/bin/env python

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64



def callback(data):
    pub = rospy.Publisher('/tilt_controller/command', Float64, queue_size=10)
    print "Load = " , data.load , " Pos = " , data.current_pos," Goal = ",data.goal_pos
    
    if(data.load < -0.25):
        pub.publish(data.current_pos-0.1)
    elif(data.load > 0.25):
        pub.publish(data.current_pos+0.1)
    
    
def listen():
    rospy.Subscriber("/tilt_controller/state", JointState, callback)
    rospy.spin()

if __name__ == "__main__":
    try:
        rospy.init_node('tile_ctr', anonymous=True)
        listen()
    except rospy.ROSInterruptException:
        pass