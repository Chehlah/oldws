#!/usr/bin/env python

import rospy
import sys
import select, termios, tty
from std_msgs.msg import Float64

def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(3)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

# main work
def arrow():
    count = 0
    input_key = input()

    while not rospy.is_shutdown():
	input_key = input()
	pub.publish(input_key)
        rate.sleep()

if __name__ == "__main__":
    try:
      
        rospy.init_node('input_key')
        pub = rospy.Publisher('force_input', Float64, queue_size=5) 
        rate = rospy.Rate(10)
        while(1):
            arrow()
    except rospy.ROSInterruptException:
        pass
