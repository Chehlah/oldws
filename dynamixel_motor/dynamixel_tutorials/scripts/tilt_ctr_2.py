#!/usr/bin/env python

import rospy
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64



def callback(data):
    pub = rospy.Publisher('/pan_controller/command', Float64, queue_size=10)
    print data.data
    
    num = 0.0
    rate = rospy.Rate(10)
    while(num < 5.0):
        num = num + data.data
        pub.publish(num)
        rate.sleep()
    num = 5.0
    while(num > 0.0):
        num = num - data.data
        pub.publish(num)
        rate.sleep()
    num = 0.0
    
def listen():
    rospy.Subscriber("force_input", Float64, callback)
    rospy.spin()

if __name__ == "__main__":
    try:
        rospy.init_node('tile_ctr', anonymous=True)
	while(1):       
	    listen()
    except rospy.ROSInterruptException:
        pass
